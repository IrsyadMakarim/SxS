extends Control

onready var slider = $HSlider

func _process(delta):
	AudioServer.set_bus_volume_db(AudioServer.get_bus_index("Master"), slider.value)
	Global.sound = true

func _on_Button_pressed():
	get_tree().change_scene("res://Scene/Menu.tscn")
