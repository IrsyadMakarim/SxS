extends StaticBody2D

var active = false

func _ready():
	$RichTextLabel.visible = active

func _input(event):
	if event.is_action_pressed("ui_accept"):
		$AnimationPlayer.play("Open")
		$Interact/CollisionShape2D.disabled = true

func _on_Interact_body_entered(body):
	if "Player" in body.name:
		active = true
		$RichTextLabel.visible = active

func _on_Interact_body_exited(body):
	if "Player" in body.name:
		active = false
		$RichTextLabel.visible = active
