extends MarginContainer

onready var selector_one = $CenterContainer/VBoxContainer/CenterContainer2/VBoxContainer/CenterContainer/HBoxContainer/Selector
onready var selector_three = $CenterContainer/VBoxContainer/CenterContainer2/VBoxContainer/CenterContainer2/HBoxContainer/Selector
onready var selector_two = $CenterContainer/VBoxContainer/CenterContainer2/VBoxContainer/CenterContainer3/HBoxContainer/Selector
onready var selecter_four = $CenterContainer/VBoxContainer/CenterContainer2/VBoxContainer/CenterContainer4/HBoxContainer/Selector

onready var mainMenuMusic = $MainMenuMusic

var current_selection = 0

func _ready():
	set_current_selection(0)
	mainMenuMusic.play()

func _process(delta):
	if Input.is_action_just_pressed("ui_down") and current_selection < 3:
		current_selection += 1
		set_current_selection(current_selection)
	elif Input.is_action_just_pressed("ui_up") and current_selection > 0:
		current_selection -= 1
		set_current_selection(current_selection)
	elif Input.is_action_just_pressed("ui_accept"):
		handle_selection(current_selection)

func handle_selection(_current_selection):
	match _current_selection:
		0:
			get_tree().change_scene(Ref.levelList.get_level())
			mainMenuMusic.stop()
			GameMusic.play()
			Global.reset()
			if (get_tree().paused == true):
				get_tree().paused = false
		1:
			get_tree().change_scene("res://Scene/UI/Credit.tscn")
		2:
			get_tree().change_scene("res://Scene/UI/Option.tscn")
		3:
			get_tree().quit()

func set_current_selection(_current_selection):
	selector_one.text = ""
	selector_two.text = ""
	selector_three.text = ""
	selecter_four.text = ""
	match _current_selection:
		0:
			selector_one.text = ">"
		1:
			selector_two.text = ">"
		2:
			selector_three.text = ">"
		3:
			selecter_four.text = ">"
