extends Node2D

var cache := []

func _ready():
	cache.push_back(load("res://Scene/Levels/Level_1.tscn"))
	cache.push_back(load("res://Scene/Levels/Level_2.tscn"))
	cache.push_back(load("res://Scene/Objects/Enemy.tscn"))
	cache.push_back(load("res://Scene/Objects/Player.tscn"))
	cache.push_back(load("res://Scene/Objects/AmmoPickup.tscn"))
	cache.push_back(load("res://Scene/Objects/HealthPickup.tscn"))
	cache.push_back(load("res://Scene/UI/HealthBar.tscn"))
