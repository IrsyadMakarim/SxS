extends Area2D

onready var sprite = $Sprite
onready var collision = $CollisionShape2D

func _ready():
	Global.connect("pickup_grenade", self, "grenade_pickup")

func grenade_pickup():
	if Global.ammo < 25:
		Global.ammo = Global.ammo + (25 - Global.ammo)
	if Global.grenade_uses < 10:
		Global.grenade_uses = Global.grenade_uses + (10 - Global.grenade_uses)
		Global.emit_signal("grenade_changed")
	if Global.ammo_smg < 75:
		Global.ammo_smg = Global.ammo_smg + (75 - Global.ammo_smg)

func _on_AmmoPickup_body_entered(body):
	if Global.grenade_uses < 10 or Global.ammo < 25 or Global.ammo_smg < 75:
		if "Player" in body.name:
			sprite.hide()
			Global.emit_signal("pickup_grenade")
			Global.emit_signal("ammo_changed")
			collision.set_deferred("disable", true)
			AudioPlayer.play("res://Audio/GrenadePickupSound.wav")
			yield(get_tree().create_timer(0.2), "timeout")
			queue_free()
