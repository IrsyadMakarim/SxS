extends Node2D

#Variable untuk load object player
onready var player = preload("res://Scene/Objects/Player.tscn")

var direction 
var dir #dir menyimpan direction 

func _ready():
	spawn_player() #Panggil function spawn_player() 

#Function untuk spawn Player
#buat object baru dari scene Player
#set posisi dengan node SpawnPositionPlayer
#dan jadiin child dari parent (Level_1)
func spawn_player() -> void:
	var spawnPlayer = player.instance()
	if (spawnPlayer != null):
		spawnPlayer.position = get_node("SpawnPositionPlayer").position
		call_deferred("add_child", spawnPlayer)
