extends KinematicBody2D
class_name Player

onready var weapon_texture = $Weapon
onready var crosshair_pos = $CrosshairPosition/PistolCrosshair
onready var crosshair_node = $CrosshairPosition
onready var crosshair_shotgun = $CrosshairPosition/ShotgunPosition

var bullet = preload("res://Scene/Objects/Bullet.tscn")
var grenade = preload("res://Scene/Objects/Grenade.tscn")

var fire_rate = 0
var died = false

var player_data = load("res://Script/PlayerModel.gd").new()

signal player_died

func _ready():
	connect("player_died", self, "update_player")
	player_data.set_move_speed(300)
	player_data.set_grenade_speed(400)
	died = false
	set_weapon_texture()

func _physics_process(delta):
	if !died and !Global.paused:
		var motion = Vector2()
		fire_rate -= delta
		
		#movement script
		if Input.is_action_pressed("up"):
			motion.y -= 1
		if Input.is_action_pressed("down"):
			motion.y += 1
		if Input.is_action_pressed("left"):
			motion.x -= 1
		if Input.is_action_pressed("right"):
			motion.x += 1
		motion = motion.normalized()
		motion = move_and_slide(motion * player_data.get_move_speed())
		
		crosshair_node = look_at(get_global_mouse_position())
		
		change_weapons()
		fire_weapons()
	
	if died:
		kill()

#set weapon texture based on the weapon the player holding
func set_weapon_texture():
	if Global.current_weapon == 1:
		weapon_texture.texture = load("res://Sprite/pistol.png")
	if Global.current_weapon == 2:
		weapon_texture.texture = load("res://Sprite/shotgun.png")
	if Global.current_weapon == 3:
		weapon_texture.texture = load("res://Sprite/smg.png")

#change the weapon based on the key binding (e.g 1 for pistol, 2 for shotgun, 3 for smg)
func change_weapons():
	if Input.is_action_just_pressed("primary"):
		Global.current_weapon = 1
		set_weapon_texture()
		Global.emit_signal("weapon_changed", Global.current_weapon)
		Global.emit_signal("ammo_changed")
		Global.emit_signal("text_changed")
		
	if Input.is_action_just_pressed("shotgun"):
		Global.current_weapon = 2
		set_weapon_texture()
		Global.emit_signal("weapon_changed", Global.current_weapon)
		Global.emit_signal("ammo_changed")
		Global.emit_signal("text_changed")
		
	if Input.is_action_just_pressed("smg"):
		Global.current_weapon = 3
		set_weapon_texture()
		Global.emit_signal("weapon_changed", Global.current_weapon)
		Global.emit_signal("ammo_changed")
		Global.emit_signal("text_changed")

#function for firing the weapon and to set the fire rate of the weapon
func fire_weapons():
	match Global.current_weapon:
		1:
			if Input.is_action_pressed("fire") and fire_rate < 0:
				fire_rate = 0.25
				AudioPlayer.play("res://Audio/Shoot.wav")
				fire()
		
		2:
			if Global.ammo > 0:
				if Input.is_action_just_pressed("fire") and fire_rate < 0:
					fire_rate = 0.4
					AudioPlayer.play("res://Audio/Shoot.wav")
					fire_shotgun()
					Global.ammo -= 1
					Global.emit_signal("ammo_changed")
			else:
				if Input.is_action_just_pressed("fire") and fire_rate < 0:
					fire_rate = 0.4
					AudioPlayer.play("res://Audio/EmptyClip.wav")
		
		3:
			if Global.ammo_smg > 0:
				if Input.is_action_pressed("fire") and fire_rate <0:
					fire_rate = 0.1
					AudioPlayer.play("res://Audio/Shoot.wav")
					fire_smg()
					Global.ammo_smg -= 1
					Global.emit_signal("ammo_changed")
			else:
				if Input.is_action_pressed("fire") and fire_rate <0:
					fire_rate = 0.1
					AudioPlayer.play("res://Audio/EmptyClip.wav")
	
	if Input.is_action_just_pressed("secondary") and Global.grenade_uses > 0:
		AudioPlayer.play("res://Audio/GrenadeShoot.wav")
		fire_grenade()
		Global.grenade_uses = Global.grenade_uses - 1
		Global.emit_signal("grenade_changed")

#function to spawn the bullet and adjusting it so it shoot straight to the cursor
func fire():
	var bullet_instance = bullet.instance()
	bullet_instance.rotation_degrees = rotation_degrees
	bullet_instance.position = crosshair_pos.global_position
	bullet_instance.velocity = get_global_mouse_position() - bullet_instance.position
	get_tree().get_root().call_deferred("add_child", bullet_instance)

#function to spawn the bullet and adjusting it so it shoot straight to the cursor
func fire_shotgun():
	for i in crosshair_shotgun.get_child_count():
		var bullet_instance = bullet.instance()
		bullet_instance.rotation_degrees = rotation_degrees 
		bullet_instance.position = crosshair_shotgun.get_child(i).global_position
		bullet_instance.velocity = get_global_mouse_position() - bullet_instance.position
		get_tree().get_root().call_deferred("add_child", bullet_instance)

#function to spawn the bullet and adjusting it so it shoot straight to the cursor
func fire_smg():
	var bullet_instance = bullet.instance()
	bullet_instance.rotation_degrees = rotation_degrees
	bullet_instance.position = crosshair_pos.global_position
	bullet_instance.velocity = get_global_mouse_position() - bullet_instance.position
	get_tree().get_root().call_deferred("add_child", bullet_instance)

#function to spawn the bullet and adjusting it so it shoot straight to the cursor
func fire_grenade():
	if Global.grenade_uses > 0:
		var grenade_instance = grenade.instance()
		grenade_instance.position = crosshair_pos.global_position
		grenade_instance.rotation_degrees = rotation_degrees
		grenade_instance.apply_impulse(Vector2(), Vector2(player_data.grenadeSpeed, 0).rotated(rotation))
		get_tree().get_root().call_deferred("add_child", grenade_instance)

func kill():
	get_tree().reload_current_scene()
	Global.reset()

func _on_Area2D_body_entered(body):
	if "Enemy" in body.name:
		AudioPlayer.play("res://Audio/Wall_hit.wav")
		player_hit(5)
	elif "Projectile" in body.name:
		player_hit(15)

func player_hit(num):
	if Global.player_health > 0:
		Global.player_health -= num
		Global.emit_signal("health_changed")
	else:
		emit_signal("player_died")

func update_player():
	died = true
