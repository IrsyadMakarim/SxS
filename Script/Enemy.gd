extends KinematicBody2D

onready var float_text = $FCTMgr
onready var sprite = $Sprite
onready var anim_player = $AnimationPlayer
onready var collision = $CollisionShape2D

var motion = Vector2()
var speed = 2
var died = false
var health: int = 50
var player = null
var playerSpotted = false
var laser_color = Color(1.0, .329, .298)

var floating_text_scene = preload("res://Scene/UI/FloatingText.tscn")
#var enemySpeed = load("res://EnemySpeed.gd").new()

var hit_pos

signal enemy_died

func _ready():
	connect("enemy_died", self, "update_enemy")

func handle_hit(num):
	float_text.show_value("-" + str(num))
	health -= num
	if health <= 0:
		anim_player.play("Explode")
		sprite.hide()
		collision.set_deferred("disabled", true)
		emit_signal("enemy_died")
		AudioPlayer.play("res://Audio/Explosion.wav")
		yield(get_tree().create_timer(0.7), "timeout")
		queue_free()

func _physics_process(delta):
	if died == false and Global.paused == false:
		if player:
			check_player()
			if playerSpotted:
				motion += (Vector2(player.position) - position)
				motion = motion.normalized()*speed
				move_and_collide(motion)
			else:
				motion = Vector2.ZERO

func check_player():
	var space_state = get_world_2d().direct_space_state
	var result = space_state.intersect_ray(position, player.position, 
	[self], collision_mask)
	
	if result:
		hit_pos = result.position
		if result.collider.name == "Player":
			playerSpotted = true

func update_enemy():
	died = true

func _on_DetectPlayer_body_entered(body):
	if !"Player" in body.name:
		return
	
	player = body

func _on_DetectPlayer_body_exited(body):
	if body == player:
		player = null
