extends Node

var levelLists = [
	"res://Scene/Levels/Level_1.tscn",
	"res://Scene/Levels/Level_2.tscn"
	]

var playedLists = []

var currentLevel

func get_length():
	return levelLists.size()

func remove_level(value):
	levelLists.remove(levelLists.find(value))

func randomize_level():
	var randomLevels
	
	if(get_length() != 0):
		randomLevels = levelLists[randi() % get_length()]
	else:
		levelLists.append_array(playedLists)
		
		randomLevels = levelLists[randi() % get_length()]
	
	currentLevel = randomLevels

func get_level():
	randomize_level()
	
	playedLists.push_back(currentLevel)
	
	remove_level(currentLevel)
	
	return currentLevel
