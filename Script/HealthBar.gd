extends CanvasLayer

onready var health_bar = $HealthBar
onready var ammo = $VBoxContainer/Ammo
onready var grenade = $VBoxContainer3/Grenade

func _ready():
	Global.connect("health_changed", self, "set_new_health_value")
	Global.connect("grenade_changed", self, "set_grenade")
	Global.connect("ammo_changed", self, "set_ammo")
	Global.connect("text_changed", self, "set_text")
	health_bar.value = Global.player_health
	set_ammo()
	set_grenade()

func set_new_health_value():
	health_bar.value = Global.player_health

func set_grenade():
	grenade.text = str(Global.grenade_uses) + "/10"

func set_ammo():
	if Global.current_weapon == 1:
		ammo.text = "∞" 
	if Global.current_weapon == 2:
		ammo.text = str(Global.ammo)
	if Global.current_weapon == 3:
		ammo.text = str(Global.ammo_smg)

func set_text():
	if Global.current_weapon == 1:
		$EquippedWeapons.text = "Equipped Pistol"
		$EquippedWeapons/AnimationPlayer.play("ShowText")
		$EquippedWeapons/Timer.start()
		yield($EquippedWeapons/Timer, "timeout")
		$EquippedWeapons/AnimationPlayer.play("HideText")
	if Global.current_weapon == 2 :
		$EquippedWeapons.text = "Equipped Shotgun"
		$EquippedWeapons/AnimationPlayer.play("ShowText")
		$EquippedWeapons/Timer.start()
		yield($EquippedWeapons/Timer, "timeout")
		$EquippedWeapons/AnimationPlayer.play("HideText")
	if Global.current_weapon == 3 :
		$EquippedWeapons.text = "Equipped SMG"
		$EquippedWeapons/AnimationPlayer.play("ShowText")
		$EquippedWeapons/Timer.start()
		yield($EquippedWeapons/Timer, "timeout")
		$EquippedWeapons/AnimationPlayer.play("HideText")
