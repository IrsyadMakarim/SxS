extends KinematicBody2D

var velocity = Vector2(1,0)
var speed = 800

func _physics_process(delta):
	
	var collision_info = move_and_collide(velocity.normalized() * delta * speed)

func _on_Area2D_body_entered(body):
	if "Enemy" in body.name:
		AudioPlayer.play("res://Audio/Wall_hit.wav")
		if(body.has_method("handle_hit")):
			body.handle_hit(30)
		queue_free()
	if "Wall" in body.name:
		queue_free()
