extends Area2D

onready var sprite = $Sprite
onready var collision = $CollisionShape2D

func _ready():
	Global.connect("pickup_health", self, "health_pickup")

func _on_HealthPickup_body_entered(body):
	if Global.player_health < 100:
		if "Player" in body.name:
			sprite.hide()
			Global.emit_signal("pickup_health")
			collision.set_deferred("disable", true)
			AudioPlayer.play("res://Audio/HealthPickupSound.wav")
			yield(get_tree().create_timer(0.2), "timeout")
			queue_free()

func health_pickup():
	if Global.player_health < 100:
		Global.player_health = Global.player_health + (100 - Global.player_health)
		Global.emit_signal("health_changed")
