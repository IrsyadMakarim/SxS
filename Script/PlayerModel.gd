extends Node

var moveSpeed : int setget set_move_speed, get_move_speed
var bulletSpeed : int setget set_bullet_speed, get_bullet_speed
var grenadeSpeed : int setget set_grenade_speed, get_grenade_speed
var fireRate : int setget set_fire_rate, get_fire_rate

func set_fire_rate(value) -> void:
	fireRate = value

func get_fire_rate() -> int:
	return fireRate

func set_move_speed(value) -> void:
	moveSpeed = value

func get_move_speed() -> int:
	return moveSpeed

func set_bullet_speed(value) -> void:
	bulletSpeed = value

func get_bullet_speed() -> int:
	return bulletSpeed

func set_grenade_speed(value) -> void:
	grenadeSpeed = value

func get_grenade_speed() -> int:
	return grenadeSpeed
