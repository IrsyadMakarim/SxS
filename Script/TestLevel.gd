extends Node2D

onready var positions = $"Spawn Position Enemies"
onready var pickups = $"Spawn Position Pickups"
onready var enemyList = $EnemyList
onready var pickupList = $PickupList

onready var enemy = preload("res://Scene/Objects/Enemy.tscn")
onready var player = preload("res://Scene/Objects/Player.tscn")
onready var ammo = preload("res://Scene/Objects/AmmoPickup.tscn")
onready var health = preload("res://Scene/Objects/HealthPickup.tscn")
onready var ui = preload("res://Scene/UI/HealthBar.tscn")

func _ready():
	spawn_player()
	spawn_pickups()
	spawn_UI()
	spawn_enemies()

func _physics_process(delta):
	check_enemy_dead()

func spawn_enemies() -> void:
	for i in positions.get_child_count():
		var spawnEnemy = randomize_enemies().instance()
		spawnEnemy.position = positions.get_child(i).position
		enemyList.add_child(spawnEnemy)

func spawn_player() -> void:
	var spawnPlayer = player.instance()
	spawnPlayer.position = get_node("SpawnPositionPlayer").position
	call_deferred("add_child", spawnPlayer)

func spawn_pickups() -> void:
	for i in pickups.get_child_count():
		var spawnPickups = randomize_pickups().instance()
		spawnPickups.position = pickups.get_child(i).position
		pickupList.add_child(spawnPickups)

func spawn_UI() -> void:
	var spawnUI = ui.instance()
	call_deferred("add_child", spawnUI)

func randomize_pickups() -> Object:
	randomize()
	
	var pickups = [ammo, health]
	var randomPickups = pickups[randi() % pickups.size()]
	
	return randomPickups

func randomize_enemies() -> Object:
	randomize()
	
	var enemies = [enemy]
	var randomEnemies = enemies[randi() % enemies.size()]
	
	return randomEnemies

func check_enemy_dead() -> void:
	if (enemyList.get_child_count() == 0):
		get_tree().change_scene(Ref.levelList.get_level())
