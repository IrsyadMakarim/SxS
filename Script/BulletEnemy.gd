extends Area2D

var speed = 300
var velocity = Vector2()

func start(pos, dir):
	position = pos
	rotation = dir
	velocity = Vector2(speed, 0).rotated(dir)

func _physics_process(delta):
	position += velocity * delta

func _on_Area2D_body_entered(body):
	if "Player" in body.name:
		AudioPlayer.play("res://Audio/Wall_hit.wav")
		queue_free()
	elif "Wall" in body.name:
		queue_free()
	elif "Enemy" in body.name: 
		AudioPlayer.play("res://Audio/Wall_hit.wav")
		queue_free()
	elif "Grenadier" in body.name:
		queue_free()

func _on_Projectile_area_entered(area):
	queue_free()
