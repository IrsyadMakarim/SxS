extends KinematicBody2D

onready var BULLET_SCENE = preload("res://Scene/Objects/Projectile.tscn")

var player = null
var move = Vector2.ZERO
var speed = 1
var fire_rate = 0.2
var bullet_speed = 300
var health: int  = 50
var died = false

var hit_pos
var can_shoot = true

signal enemy_died

func _ready():
	connect("enemy_died", self, "update_enemy")
	
	$ShootTimer.wait_time = fire_rate

func update_enemy():
	died = true

func _physics_process(delta):
	if died == false and Global.paused == false:
		move = Vector2.ZERO
		fire_rate -= delta
		
		if player:
			look_at(player.position)
#			move = position.direction_to(player.position) * speed
			check_player()
		else:
			move = Vector2.ZERO
		
		move = move.normalized()
		move = move_and_collide(move)

func check_player():
	var space_state = get_world_2d().direct_space_state
	var result = space_state.intersect_ray(position, player.position, 
	[self])
	
	if result:
		hit_pos = result.position
		if result.collider.name == "Player":
			look_at(player.position)
			if can_shoot:
				fire(player.position)

func handle_hit(num):
	$FCTMgr.show_value("-" + str(num))
	health -= num
	print("Enemy hit", health)
	if health <= 0:
		$AnimationPlayer.play("Explode")
		$Sprite.hide()
		$CollisionShape2D.set_deferred("disabled", true)
		$DetectShot/CollisionShape2D.set_deferred("disabled", true)
		emit_signal("enemy_died")
		Explosion.position = position
		Explosion.play()
		yield(get_tree().create_timer(0.7), "timeout")
		queue_free()

func _on_Area2D_body_entered(body):
	if !"Player" in body.name:
		return
	
	player = body

func _on_Area2D_body_exited(body):
	if body == player:
		player = null

func fire(pos):
	var bullet = BULLET_SCENE.instance()
	var bulletPos = (pos - global_position).angle()
	bullet.start(global_position, bulletPos + rand_range(-0.05, 0.05))
	call_deferred("add_child", bullet)
	Shoot.position = position
	Shoot.play()
	can_shoot = false
	$ShootTimer.start()

func _on_DetectShot_body_entered(body):
	if "Bullet" in body.name:
		handle_hit(25)

func _on_ShootTimer_timeout():
	can_shoot = true
